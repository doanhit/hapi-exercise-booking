
const service = require('../services/booking.js');

module.exports =[ {
    method: 'POST',
    path: '/bookings/create',
    config: {
      tags: ['api'],
      // auth: ,
      handler: service.createBooking,
      validate: {
        payload: service.validateSchema
    }
    }
  },
  {
  method: 'PUT',
  path: `/bookings/update/{id}`,
  config: {
    tags: ['api'],
    handler: service.updateBooking
  }},
  {
    method: 'GET',
    path: `/bookings/findByStatus`,
    config: {
      tags: ['api'],
      handler: service.findBookingByStatus
    }},
    {
      method: 'GET',
      path: `/bookings/{bookingId}`,
      config: {
        tags: ['api'],
        handler: service.findBookingById
      }}
]