'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bookingSchema = new Schema({
    name: {
        type: String,
        required: [true, 'name is required'],
      },
      fromAddress: {
        type: String,
        required: [true, 'fromAddress is required']
      },
      toAddress: {
        type: String,
        required: [true, 'toAddress is required']
      },
      note: {
        type: String,
      },
      status: {
        type: String,
        required: [true, 'status is required'],
        enum : ['booked','pending','in-transit','completed'],
        default: 'booked'
      },
      userId: {
        type: String,
        required: [true, 'userId is required']
      },
    
});
const BookingModel = mongoose.model('Booking', bookingSchema);

  const createBooking =  (booking,cb) =>  BookingModel.create(booking,cb);
  const updateBooking =  (id, data, cb) =>  BookingModel.findByIdAndUpdate({_id:id}, data,{new : true} , cb);
  const findBookingById =  (idBooking, cb) =>  BookingModel.find({_id:idBooking}, cb);
  const findBookingByStatus =  (statusBooking, cb) =>  BookingModel.find(statusBooking, cb);

module.exports = {
  createBooking,
  updateBooking,
  findBookingById,
  findBookingByStatus
}